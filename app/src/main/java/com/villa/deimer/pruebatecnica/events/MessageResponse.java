package com.villa.deimer.pruebatecnica.events;

import com.villa.deimer.pruebatecnica.models.SchoolBus;
import java.util.List;

/**
 * Creado por Deimer Villa on 2/16/18.
 */

public class MessageResponse {

    private boolean success;
    private String message;
    private int option;
    private SchoolBus schoolBus;
    private List<SchoolBus> buses;

    public MessageResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
    }
    public MessageResponse(boolean success, String message, int option) {
        this.success = success;
        this.message = message;
        this.option = option;
    }
    public MessageResponse(boolean success, String message, List<SchoolBus> buses) {
        this.success = success;
        this.message = message;
        this.buses = buses;
    }
    public MessageResponse(boolean success, String message, int option, SchoolBus schoolBus) {
        this.success = success;
        this.message = message;
        this.option = option;
        this.schoolBus = schoolBus;
    }
    public MessageResponse(boolean success, String message, int option, List<SchoolBus> buses) {
        this.success = success;
        this.message = message;
        this.option = option;
        this.buses = buses;
    }

    public boolean isSuccess() {
        return success;
    }
    public String getMessage() {
        return message;
    }
    public int getOption() {
        return option;
    }
    public SchoolBus getSchoolBus() {
        return schoolBus;
    }
    public List<SchoolBus> getBuses() {
        return buses;
    }

}
