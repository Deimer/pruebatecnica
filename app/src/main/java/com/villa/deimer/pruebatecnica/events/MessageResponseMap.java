package com.villa.deimer.pruebatecnica.events;

import com.villa.deimer.pruebatecnica.models.Stop;
import java.util.List;

/**
 * Creado por Deimer Villa on 2/16/18.
 */

public class MessageResponseMap {

    private boolean success;
    private String message;
    private List<Stop> stops;

    public MessageResponseMap(boolean success, String message, List<Stop> stops) {
        this.success = success;
        this.message = message;
        this.stops = stops;
    }
    public MessageResponseMap(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }
    public String getMessage() {
        return message;
    }
    public List<Stop> getStops() {
        return stops;
    }

}
