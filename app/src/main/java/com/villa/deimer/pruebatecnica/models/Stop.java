package com.villa.deimer.pruebatecnica.models;

import com.google.gson.annotations.SerializedName;

/**
 * Creado por Deimer Villa on 2/16/18.
 */

public class Stop {

    @SerializedName("lat")
    private float latitude;
    @SerializedName("lng")
    private float longitude;

    public Stop() {}
    public Stop(float latitude, float longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    //Getters
    public float getLatitude() {
        return latitude;
    }
    public float getLongitude() {
        return longitude;
    }

    //Setters
    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }
    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    //ToString
    @Override
    public String toString() {
        return "Stop{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
