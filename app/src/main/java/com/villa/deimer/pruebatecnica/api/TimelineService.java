package com.villa.deimer.pruebatecnica.api;

import android.content.Context;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.villa.deimer.pruebatecnica.events.MessageResponse;
import com.villa.deimer.pruebatecnica.events.StationEvent;
import com.villa.deimer.pruebatecnica.models.SchoolBus;
import java.util.ArrayList;
import java.util.List;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Creado por Deimer Villa on 2/16/18.
 */

public class TimelineService {

    private Context context;

    public TimelineService(Context context) {
        this.context = context;
    }

    public void getData() {
        final String url = "https://api.myjson.com";
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(url)
                .build();
        Api api = restAdapter.create(Api.class);
        api.getData(new Callback<JsonObject>() {
            @Override
            public void success(JsonObject jsonObject, Response response) {
                boolean success = jsonObject.get("response").getAsBoolean();
                if (success) {
                    JsonArray array_buses = jsonObject.get("school_buses").getAsJsonArray();
                    List<SchoolBus> buses = convertObjects(array_buses);
                    StationEvent.getBus().post(new MessageResponse(true, "", 1, buses));
                }
            }
            @Override
            public void failure(RetrofitError error) {
                StationEvent.getBus().post(new MessageResponse(false, error.getMessage()));
                try {
                    Log.d("TimelineView(getData)", "Errors: " + error.getBody().toString());
                    Log.d("TimelineView(getData)", "Errors body: " + error.getBody().toString());
                } catch (Exception ex) {
                    Log.e("TimelineView(getData)", "Error ret: " + error + "; Error ex: " + ex.getMessage());
                }
            }
        });
    }

    private List<SchoolBus> convertObjects(JsonArray array) {
        List<SchoolBus> buses = new ArrayList<>();
        if(array.size() > 0) {
            for (int i = 0; i < array.size(); i++) {
                JsonObject json = array.get(i).getAsJsonObject();
                SchoolBus bus = new Gson().fromJson(json, SchoolBus.class);
                System.out.println(bus.toString());
                buses.add(bus);
            }
        }
        return buses;
    }

}
