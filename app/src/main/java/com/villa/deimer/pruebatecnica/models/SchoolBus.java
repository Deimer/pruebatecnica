package com.villa.deimer.pruebatecnica.models;

import java.io.Serializable;

/**
 * Creado por Deimer Villa on 2/16/18.
 */

public class SchoolBus implements Serializable {

    //Atributos
    private int id;
    private String name;
    private String description;
    private String stops_url;
    private String img_url;

    //Constructores
    public SchoolBus() {}
    public SchoolBus(int id, String name, String description, String stops_url, String img_url) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.stops_url = stops_url;
        this.img_url = img_url;
    }

    //Getters
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getDescription() {
        return description;
    }
    public String getStops_url() {
        return stops_url;
    }
    public String getImg_url() {
        return img_url;
    }

    //Setters
    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setStops_url(String stops_url) {
        this.stops_url = stops_url;
    }
    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    //ToString
    @Override
    public String toString() {
        return "SchoolBus{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", stops_url='" + stops_url + '\'' +
                ", img_url='" + img_url + '\'' +
                '}';
    }

}
