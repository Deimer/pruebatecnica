package com.villa.deimer.pruebatecnica.api;

import com.google.gson.JsonObject;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Creado por Deimer Villa on 2/16/18.
 */

public interface Api {

    @GET("/bins/10yg1t")
    void getData(
            Callback<JsonObject> cb
    );

    @GET("/")
    void getCoordinates(
            Callback<JsonObject> cb
    );

}
