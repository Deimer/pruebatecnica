package com.villa.deimer.pruebatecnica.api;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.villa.deimer.pruebatecnica.events.MessageResponseMap;
import com.villa.deimer.pruebatecnica.events.StationEvent;
import com.villa.deimer.pruebatecnica.models.Stop;
import java.util.ArrayList;
import java.util.List;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Creado por Deimer Villa on 2/16/18.
 */

public class MapsService {

    private Context context;
    private String endpoint;

    public MapsService(Context context, String endpoint) {
        this.context = context;
        this.endpoint = endpoint;
    }

    public void getCoordinates() {
        final String url = endpoint;
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(url)
                .build();
        Api api = restAdapter.create(Api.class);
        api.getCoordinates(new Callback<JsonObject>() {
            @Override
            public void success(JsonObject jsonObject, Response response) {
                boolean success = jsonObject.get("response").getAsBoolean();
                if (success) {
                    JsonArray array = jsonObject.get("stops").getAsJsonArray();
                    List<Stop> stops = getStops(array);
                    StationEvent.getBus().post(new MessageResponseMap(true, "Ruta cargada", stops));
                }
            }
            @SuppressLint("LongLogTag") @Override
            public void failure(RetrofitError error) {
                StationEvent.getBus().post(new MessageResponseMap(false, error.getMessage()));
                try {
                    Log.d("MapsService(getCoordinates)", "Errors: " + error.getBody().toString());
                    Log.d("MapsService(getCoordinates)", "Errors body: " + error.getBody().toString());
                } catch (Exception ex) {
                    Log.e("MapsService(getCoordinates)", "Error ret: " + error + "; Error ex: " + ex.getMessage());
                }
            }
        });
    }

    private List<Stop> getStops(JsonArray array) {
        List<Stop> stops = new ArrayList<>();
        for (int i = 0; i < array.size(); i++) {
            JsonObject jsonStop = array.get(i).getAsJsonObject();
            Stop stop = new Gson().fromJson(jsonStop, Stop.class);
            stops.add(stop);
        }
        return stops;
    }

}
