package com.villa.deimer.pruebatecnica.views;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.villa.deimer.pruebatecnica.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WelcomeActivity extends AppCompatActivity {

    //Componentes
    private Context context;
    //Vistas
    @BindView(R.id.img_icon_app)ImageView icon_app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
        setupActivity();
    }

    private void setupActivity() {
        context = this;
        setupIconAnimation();
        setupTime();
    }

    private void setupIconAnimation() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                YoYo.with(Techniques.FadeInUp)
                        .duration(1000)
                        .playOn(icon_app);
                icon_app.setVisibility(View.VISIBLE);
            }
        }, 1000);
    }

    private void setupTime() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(context, TimelineActivity.class));
                finish();
            }
        }, 3500);
    }

}
