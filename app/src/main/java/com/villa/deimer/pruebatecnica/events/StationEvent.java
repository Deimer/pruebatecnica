package com.villa.deimer.pruebatecnica.events;

import com.squareup.otto.Bus;

/**
 * Creado por Deimer Villa on 2/16/18.
 */

public class StationEvent {

    private static Bus bus = new Bus();
    public static Bus getBus() {
        return bus;
    }

}
