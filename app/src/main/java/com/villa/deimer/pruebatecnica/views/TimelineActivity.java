package com.villa.deimer.pruebatecnica.views;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.squareup.otto.Subscribe;
import com.villa.deimer.pruebatecnica.R;
import com.villa.deimer.pruebatecnica.adapters.SchoolBusAdapter;
import com.villa.deimer.pruebatecnica.api.TimelineService;
import com.villa.deimer.pruebatecnica.events.MessageResponse;
import com.villa.deimer.pruebatecnica.events.StationEvent;
import com.villa.deimer.pruebatecnica.models.SchoolBus;
import com.villa.deimer.pruebatecnica.utils.MaterialDialog;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TimelineActivity extends AppCompatActivity {

    //Componentes
    private Context context;
    private TimelineService timelineService;
    private MaterialDialog dialog;
    //Elementos
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.lbl_data_not_found)
    TextView lbl_data_not_found;
    @BindView(R.id.pb_loading)
    ProgressBar pb_loading;
    @BindView(R.id.recycler_school_buses)
    RecyclerView recycler_school_buses;

    @Override
    public void onStart() {
        super.onStart();
        StationEvent.getBus().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        StationEvent.getBus().unregister(this);
    }

    @Subscribe
    public void recievedMessage(MessageResponse messageBusResponse){
        boolean success = messageBusResponse.isSuccess();
        int option = messageBusResponse.getOption();
        if(success) {
            if(option == 1) {
                List<SchoolBus> buses = messageBusResponse.getBuses();
                animateProgressBar(true);
                setupRecycler(buses);
            } else if(option == 2) {
                if(isMapConnect()) {
                    SchoolBus bus = messageBusResponse.getSchoolBus();
                    openMapActivity(bus);
                }
            }
        } else {
            animateProgressBar(true);
            String message = messageBusResponse.getMessage();
            dialog.dialogWarnings("Error", message);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);
        ButterKnife.bind(this);
        setupActivity();
    }

    private void setupActivity() {
        context = this;
        timelineService = new TimelineService(context);
        dialog = new MaterialDialog(context);
        setupToolbar();
    }

    public void setupToolbar() {
        toolbar.setTitle("Rutas");
        setSupportActionBar(toolbar);
        timelineService.getData();
    }

    private void setupRecycler(List<SchoolBus> buses) {
        if(buses.isEmpty()) {
            animateProgressBar(true);
            lbl_data_not_found.setVisibility(View.VISIBLE);
        } else {
            lbl_data_not_found.setVisibility(View.GONE);
            LinearLayoutManager layout_manager = new LinearLayoutManager(context);
            layout_manager.setOrientation(LinearLayoutManager.VERTICAL);
            SchoolBusAdapter adapter = new SchoolBusAdapter(context, buses);
            recycler_school_buses.setLayoutManager(layout_manager);
            recycler_school_buses.setAdapter(adapter);
            animateRecycler();
        }
    }

    private void animateProgressBar(boolean flag) {
        if(flag) {
            YoYo.with(Techniques.FadeOut).duration(400).playOn(pb_loading);
            pb_loading.setVisibility(View.GONE);
        } else {
            YoYo.with(Techniques.FadeIn).duration(400).playOn(pb_loading);
            pb_loading.setVisibility(View.VISIBLE);
        }
    }

    private void animateRecycler() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                YoYo.with(Techniques.FadeIn).duration(1000).playOn(recycler_school_buses);
                recycler_school_buses.setVisibility(View.VISIBLE);
            }
        }, 800);
    }

    private boolean isMapConnect() {
        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        if(available == ConnectionResult.SUCCESS) {
            Log.d("", "Google Services Ok");
            return true;
        } else if(GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            Log.d("", "Google Services is not Ok");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(TimelineActivity.this, available, 9001);
            dialog.show();
        } else {
            dialog.dialogWarnings("Error", "Error al obtener la información del mapa");
        }
        return false;
    }

    private void openMapActivity(SchoolBus bus) {
        Intent intent = new Intent(context, MapsActivity.class);
        intent.putExtra("bus", bus);
        startActivity(intent);
    }

}
