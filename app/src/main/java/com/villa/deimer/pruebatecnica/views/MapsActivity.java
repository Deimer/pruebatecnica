package com.villa.deimer.pruebatecnica.views;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.otto.Subscribe;
import com.villa.deimer.pruebatecnica.R;
import com.villa.deimer.pruebatecnica.api.MapsService;
import com.villa.deimer.pruebatecnica.events.MessageResponseMap;
import com.villa.deimer.pruebatecnica.events.StationEvent;
import com.villa.deimer.pruebatecnica.models.SchoolBus;
import com.villa.deimer.pruebatecnica.models.Stop;
import com.villa.deimer.pruebatecnica.utils.MaterialDialog;

import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    //Componentes
    private GoogleMap mMap;
    private MaterialDialog dialog;

    @Override
    public void onStart() {
        super.onStart();
        StationEvent.getBus().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        StationEvent.getBus().unregister(this);
    }

    @Subscribe
    public void recievedMessage(MessageResponseMap messageResponseMap){
        boolean success = messageResponseMap.isSuccess();
        dialog.cancelProgress();
        if(success) {
            List<Stop> stops = messageResponseMap.getStops();
            printStops(stops);
        } else {
            String message = messageResponseMap.getMessage();
            dialog.dialogWarnings("Error", message);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        dialog = new MaterialDialog(MapsActivity.this);
    }

    private SchoolBus getBundleExtra() {
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        return (SchoolBus) extras.getSerializable("bus");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng bogota = new LatLng(4.6097, -74.0817);
        float zoom = 12.0f;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bogota, zoom));
        mMap.getMinZoomLevel();
        getCoordinates();
    }

    private void getCoordinates() {
        dialog.dialogProgress("Obteniendo ruta...");
        Context context = this;
        SchoolBus bus = getBundleExtra();
        MapsService mapsService = new MapsService(context, bus.getStops_url());
        mapsService.getCoordinates();
    }

    private void printStops(List<Stop> stops) {
        if(!stops.isEmpty()) {
            mMap.clear();
            Stop stop_one = stops.get(0);
            Stop stop_two = stops.get(1);
            System.out.println(stop_one.toString() + "; " + stop_two.toString());
            printRoute(stop_one, stop_two);
            for (int i = 1; i < stops.size(); i++) {
                if(stops.size() > i+1) {
                    Stop stop_three = stops.get(i);
                    Stop stop_four = stops.get(i+1);
                    System.out.println(stop_three.toString() + "; " + stop_four.toString());
                    printRoute(stop_three, stop_four);
                }
            }
            LatLng start;
            start = new LatLng(stop_one.getLatitude(), stop_one.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(start, 14));
            mMap.getMinZoomLevel();
            mMap.addMarker(new MarkerOptions().position(new LatLng(stop_one.getLatitude(), stop_one.getLongitude())).title("Inicio"));
        }
    }

    private void printRoute(Stop stop_one, Stop stop_two) {
        LatLng pos1, pos2;
        pos1 = new LatLng(stop_one.getLatitude(), stop_one.getLongitude());
        pos2 = new LatLng(stop_two.getLatitude(), stop_one.getLongitude());
        mMap.addPolyline(new PolylineOptions().add(pos1, pos2).width(12).color(Color.BLUE));
    }

}
