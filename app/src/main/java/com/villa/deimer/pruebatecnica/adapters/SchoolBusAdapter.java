package com.villa.deimer.pruebatecnica.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.squareup.picasso.Picasso;
import com.villa.deimer.pruebatecnica.R;
import com.villa.deimer.pruebatecnica.events.MessageResponse;
import com.villa.deimer.pruebatecnica.events.StationEvent;
import com.villa.deimer.pruebatecnica.models.SchoolBus;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Creado por Deimer Villa on 2/16/18.
 *
 */

public class SchoolBusAdapter extends RecyclerView.Adapter<SchoolBusAdapter.AdapterView> {

    private Context context;
    private List<SchoolBus> buses;

    public SchoolBusAdapter(Context context, List<SchoolBus> buses) {
        this.context = context;
        this.buses = buses;
    }

    @Override
    public AdapterView onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_school_buses, parent, false);
        return new AdapterView(layoutView);
    }

    @Override
    public int getItemCount() {
        return buses.size();
    }

    class AdapterView extends RecyclerView.ViewHolder {
        @BindView(R.id.card_item)
        CardView card_item;
        @BindView(R.id.img_item)
        CircleImageView img_item;
        @BindView(R.id.lbl_name)
        TextView lbl_name;
        @BindView(R.id.lbl_description)
        TextView lbl_description;
        AdapterView(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onBindViewHolder(AdapterView holder, int position) {
        SchoolBus bus = buses.get(position);
        setupLabels(holder, bus);
        loadImage(holder.img_item, bus);
        animateCard(holder);
        tapCardInfo(holder, bus);
    }

    private void setupLabels(AdapterView holder, SchoolBus bus) {
        holder.lbl_name.setText(bus.getName());
        holder.lbl_description.setText(bus.getDescription());
    }

    private void loadImage(CircleImageView img_view, SchoolBus bus) {
        System.out.println(bus.getImg_url());
        Picasso.with(context)
                .load(bus.getImg_url())
                .fit()
                .centerInside()
                .placeholder(R.drawable.img_default)
                .error(R.drawable.img_default)
                .into(img_view);
    }

    private void animateCard(AdapterView holder){
        YoYo.with(Techniques.SlideInDown).duration(800).playOn(holder.card_item);
    }

    private void tapCardInfo(AdapterView holder, final SchoolBus bus) {
        holder.card_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StationEvent.getBus().post(new MessageResponse(true, "", 2, bus));
            }
        });
    }

}
